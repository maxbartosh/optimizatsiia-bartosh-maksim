﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;

namespace PR_1_Dichotomies
{
    public class Program
    {
        public static string SettingsFileName = "settings.json";
        
        static void Main(string[] args)
        {
            var settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(SettingsFileName));

            decimal a = settings.A;
            decimal b = settings.B;
            decimal epsilon = settings.Epsilon;
            string function = settings.Function;
            
            var res1= Dichotomies(a, b, function, epsilon);
            
            var res2= GoldenSection(a, b, function, epsilon);
            
            var res3= Fibonacci(a, b, function, epsilon);
            
            Console.WriteLine($"Dichotomies {res1}; GoldenSection {res2}; Fibonacci {res3}");
            
        }

        public static decimal Dichotomies(decimal a, decimal b, string function, decimal epsilon)
        {
            decimal x = 0, f1, f2;

            var dt = new DataTable();

            while (Math.Abs(b - a) > epsilon)
            {
                x = (a + b) / 2;

                f1 = (decimal) dt.Compute(function.Replace("x", (x - epsilon).ToString(CultureInfo.InvariantCulture)),
                    "");
                f2 = (decimal) dt.Compute(function.Replace("x", (x + epsilon).ToString(CultureInfo.InvariantCulture)),
                    "");

                if (f1 >= f2)
                {
                    a = x;
                }
                else
                {
                    b = x;
                }
            }

            return x;
        }

        public static decimal GoldenSection(decimal a, decimal b, string function, decimal epsilon)
        {
            decimal f1, f2;

            var dt = new DataTable();

            decimal gr = (decimal) (Math.Sqrt(5) + 1) / 2;
            while (Math.Abs(b - a) > epsilon)
            {
                decimal x1 = b - (b - a) / gr;
                decimal x2 = a + (b - a) / gr;

                f1 = (decimal) dt.Compute(function.Replace("x", (x1 - epsilon).ToString(CultureInfo.InvariantCulture)),
                    "");
                f2 = (decimal) dt.Compute(function.Replace("x", (x2 + epsilon).ToString(CultureInfo.InvariantCulture)),
                    "");

                if (f1 >= f2)
                {
                    a = x1;
                }
                else
                {
                    b = x2;
                }
            }

            return (b + a) / 2;
        }

        public static decimal Fibonacci(decimal a, decimal b, string function, decimal epsilon)
        {
            decimal x = 0, f1, f2;

            var dt = new DataTable();

            int n = 0;

            while (Fib(n) < (b - a) / 2 * epsilon)
            {
                n++;
            }

            decimal x1, x2;

            while (n != 1)
            {
                int n1 = Fib(n - 1);
                int n2 = Fib(n - 2);
                int tempN = Fib(n);
                x1 = a + (b - a) * (n2 / tempN);
                x2 = a + (b - a) * (n1 / tempN);
                f1 = (decimal) dt.Compute(function.Replace("x", (x1 - epsilon).ToString(CultureInfo.InvariantCulture)),
                    "");
                f2 = (decimal) dt.Compute(function.Replace("x", (x2 + epsilon).ToString(CultureInfo.InvariantCulture)),
                    "");
                n = n - 1;

                if (f1 >= f2)
                {
                    a = x1;
                }
                else
                {
                    b = x2;
                }

                n--;
            }

            return x;
        }
        
        public static int Fib(int n)
        {
            return n > 1 ? Fib(n - 1) + Fib(n - 2) : n;
        }
    }

    public class Settings
    {
        public decimal A { get; set; }
        public decimal B { get; set; }
        public decimal Epsilon { get; set; }
        public string Function { get; set; }
    }
}