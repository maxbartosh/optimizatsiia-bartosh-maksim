﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;

namespace PR2_PointEstimationMethods
{
    class Program
    {
        public static string SettingsFileName = "settings.json";

        static void Main(string[] args)
        {
            var settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(SettingsFileName));

            decimal a = settings.A;
            decimal b = settings.B;
            decimal epsilon = settings.Epsilon;
            string function = settings.Function;

            var res1 = QuadraticApproximation(a, b, function, epsilon);
            var res2 = CubicApproximation(a, b, function, settings.DiffFunction, epsilon);


            Console.WriteLine($"QuadraticApproximation {res1}; CubicApproximation {res2}");
        }
        
        public static decimal QuadraticApproximation(decimal a, decimal b, string function, decimal epsilon)
        {
            decimal x1 = a, x3 = b;
            decimal x2 = (x1 + x3) / 2;

            decimal res = x2;
            
            var dt = new DataTable();

            decimal f1, f2, f3,
                a0, a1, a2;
            
            while (Math.Abs(x3 - x1) > epsilon)
            {
                f1 = (decimal) dt.Compute(function.Replace("x", (x1).ToString("F6")),
                    "");
                f2 = (decimal) dt.Compute(function.Replace("x", (x2).ToString("F6")),
                    "");
                f3 = (decimal) dt.Compute(function.Replace("x", (x3).ToString("F6")),
                    "");

                a0 = f1;
                a1 = (f2 - f1) / (x2 - x1);
                a2 = (1 / (x3 - x2)) * ((f3 - f1) / (x3 - x1) - (f2 - f1) / (x2 - x1));
                res = (x2 + x1) / 2 - a1 / (2 * a2);
                
                if (res < x2)
                {
                    x3 = x2;
                    x2 = res;
                }
                else
                {
                    x1 = x2;
                    x2 = res;
                }
            }

            return res;
        }

        public static decimal CubicApproximation(decimal a, decimal b, string function, string diffFunction, decimal epsilon)
        {
            decimal x1 = a, x3 = b;
            decimal x2 = (x1 + x3) / 2;
            decimal res = x2;
            
            var dt = new DataTable();
            
            decimal f1 = (decimal) dt.Compute(function.Replace("x", (x1).ToString("F6")),
                "");
            decimal f2 =  (decimal) dt.Compute(function.Replace("x", (x2).ToString("F6")),
                "");;
            decimal diffF1 =  (decimal) dt.Compute(diffFunction.Replace("x", (x1).ToString("F6")),
                "");
            decimal diffF2 = (decimal) dt.Compute(diffFunction.Replace("x", (x2).ToString("F6")),
                "");

            decimal z = (3 * (f1 - f2) / (x2 - x1)) + diffF1 + diffF2;

            decimal w = (decimal) Math.Sqrt((double)(z * z - diffF1 * diffF2));
            
            if (x1 >= x2)
            {
                w *= -1;
            }

            var μ = (diffF2 + w - z) / (diffF2 - diffF1 + 2 * w);

            res = μ < 0 ? x2 : μ >= 0 && μ <= 1 ? x2 - μ * (x2 - x1) : x1;

            return res;
        }
    }

    public class Settings
    {
        public decimal A { get; set; }
        public decimal B { get; set; }
        public decimal Epsilon { get; set; }
        public string Function { get; set; }
        public string DiffFunction { get; set; }
    }
}